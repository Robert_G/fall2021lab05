public class NumberedCard extends ColoredCard{
    private int number;

    public NumberedCard(int number, String color){
        super(color);
        this.number = number;
    }

    public int getNumber(){
        return this.number;
    }
    public boolean canPlay(Uno card) {
        if(card instanceof RainbowCard){
            return true;
        }
        else{
            if(card instanceof NumberedCard){
                if(this.getNumber() == ((NumberedCard)card).getNumber()){
                    return true;
                }
            }

            if(this.getColor().equals(((ColoredCard)card).getColor())){
                    return true;
            }
            return false;
        }
    }

}