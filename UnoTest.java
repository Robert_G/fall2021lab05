import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class UnoTest {
    @Test
    public void canPlayColorTestPass(){
        Uno card1 = new NumberedCard(5, "blue");
        Uno card2 = new PickUp2Card("blue");
        assertEquals(true, card1.canPlay(card2));
    }

    @Test
    public void canPlayColorTestFail(){
        Uno card1 = new NumberedCard(5, "green");
        Uno card2 = new NumberedCard(1, "blue");
        assertEquals(false, card1.canPlay(card2));
    }

    @Test
    public void canPlayNumberTestPass(){
        Uno card1 = new NumberedCard(7, "green");
        Uno card2 = new NumberedCard(7, "blue");
        assertEquals(true, card1.canPlay(card2));
    }

    @Test
    public void canPlayNumberTestFail(){
        Uno card1 = new NumberedCard(5, "green");
        Uno card2 = new NumberedCard(2, "blue");
        assertEquals(false, card1.canPlay(card2));
    }

    @Test
    public void canPlaySpecialsTestPass(){
        Uno card1 = new ReverseCard("green");
        Uno card2 = new SkipCard("green");
        assertEquals(true, card1.canPlay(card2));
    }

    @Test
    public void canPlaySpecialsTestFail(){
        Uno card1 = new ReverseCard("green");
        Uno card2 = new PickUp2Card("yellow");
        assertEquals(false, card1.canPlay(card2));
    }

    @Test
    public void canPlayRainbowTestPass1(){
        Uno card1 = new ReverseCard("green");
        Uno card2 = new WildCard();
        assertEquals(true, card1.canPlay(card2));
    }

    @Test
    public void canPlayRainbowTestPass2(){
        Uno card1 = new NumberedCard(5,"red");
        Uno card2 = new WildPickUp4Card();
        assertEquals(true, card1.canPlay(card2));
    }

    @Test
    public void canPlayRainbowTestPass3(){
        Uno card2 = new NumberedCard(5,"red");
        Uno card1 = new WildPickUp4Card();
        assertEquals(true, card1.canPlay(card2));
    }

    @Test
    public void drawTest(){
        Deck deck = new Deck();
        Uno card = deck.draw();
        assertEquals(true, card instanceof RainbowCard);
    }

    @Test
    public void addToDeckTest1(){
        Deck deck = new Deck();
        Uno card = new ReverseCard("yellow");
        deck.addToDeck(card);
        assertEquals(41, deck.getSize());
    }

    @Test
    public void addToDeckTest2(){
        Deck deck = new Deck();
        Uno card = new ReverseCard("yellow");
        deck.addToDeck(card);
        assertEquals(card, deck.getCard(40));
    }
}
