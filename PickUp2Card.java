public class PickUp2Card extends ColoredCard {

    public PickUp2Card(String color){
        super(color);
    }
    public boolean canPlay(Uno card) {
        if(card instanceof RainbowCard){
            return true;
        }
        else{
            if(this.getColor().equals(((ColoredCard)card).getColor())){
                return true;
            }
            return false;
        }
};
}
