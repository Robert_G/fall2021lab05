public class ReverseCard extends ColoredCard {

    public ReverseCard(String color){
        super(color);
    }
    
    public boolean canPlay(Uno card) {
        if(card instanceof RainbowCard){
            return true;
        }
        else{
            if(this.getColor().equals(((ColoredCard)card).getColor())){
                return true;
            }
            return false;
        }
    }
    
}
