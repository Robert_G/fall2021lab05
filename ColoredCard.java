public abstract class ColoredCard implements Uno {
    private String color;
    public ColoredCard(String color){
        this.color = color;
    }
    public String getColor(){
        return this.color;
    }
}
