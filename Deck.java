import java.util.ArrayList;
public class Deck {
    private ArrayList<Uno> deck = new ArrayList<Uno>();
    
    public Deck(){
        String[] colors = new String[]{"red", "blue", "green", "yellow"};
        
        for(int i = 0; i < colors.length; i++){
            deck.add(new WildCard());
            deck.add(new WildPickUp4Card());
            deck.add(new NumberedCard(1,colors[i]));
            deck.add(new NumberedCard(2,colors[i]));
            deck.add(new PickUp2Card(colors[i]));
            deck.add(new PickUp2Card(colors[i]));
            deck.add(new ReverseCard(colors[i]));
            deck.add(new ReverseCard(colors[i]));
            deck.add(new SkipCard(colors[i]));
            deck.add(new SkipCard(colors[i]));
        }
    }

    public void addToDeck(Uno card){
        this.deck.add(card);
    }

    public int getSize(){
        return this.deck.size();
    }

    public Uno draw(){
        Uno topCard = this.deck.get(0);
        this.deck.remove(0);
        return topCard;
    }

    public Uno getCard(int position){
        return this.deck.get(position);
    }
}
